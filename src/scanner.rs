#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TokenType {
    // Single-character tokens.
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,
    // One or two character tokens.
    Bang,
    BangEq,
    Equal,
    EqEq,
    Greater,
    GreaterEq,
    Less,
    LessEq,
    // Literals.
    Identifier,
    String,
    Number,
    // Keywords.
    And,
    Class,
    Else,
    False,
    For,
    Fun,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,

    Error,
    Eof,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Token {
    pub kind: TokenType,
    pub line: usize,
    pub start: usize,
    pub end: usize,
}

impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {}:{}", self.kind, self.line, self.start)
    }
}

pub struct Scanner<'source> {
    source: &'source str,
    start: usize,
    current: usize,
    line: usize,
}

impl<'source> Scanner<'source> {
    /// Build a new scanner for the given source code
    pub fn new(code: &'source str) -> Scanner<'source> {
        Scanner {
            source: code,
            start: 0,
            current: 0,
            line: 0,
        }
    }

    fn get_source(&self) -> String {
        self.source[self.start..self.current].to_string()
    }

    fn make_token(&self, kind: TokenType) -> Token {
        Token {
            kind,
            line: self.line,
            start: self.start,
            end: self.current,
        }
    }

    /// Scan the next token
    pub fn scan_token(&mut self) -> Token {
        self.skip_comemnts_and_whitespace();
        self.start = self.current;
        if self.is_at_end() {
            return self.make_token(TokenType::Eof);
        }

        match self.advance() {
            Some('(') => self.make_token(TokenType::LeftParen),
            Some(')') => self.make_token(TokenType::RightParen),
            Some('{') => self.make_token(TokenType::LeftBrace),
            Some('}') => self.make_token(TokenType::RightBrace),
            Some(',') => self.make_token(TokenType::Comma),
            Some('.') => self.make_token(TokenType::Dot),
            Some('-') => self.make_token(TokenType::Minus),
            Some('+') => self.make_token(TokenType::Plus),
            Some(';') => self.make_token(TokenType::Semicolon),
            Some('/') => self.make_token(TokenType::Slash),
            Some('*') => self.make_token(TokenType::Star),
            Some('!') if self.matches('=') => self.make_token(TokenType::BangEq),
            Some('!') => self.make_token(TokenType::Bang),
            Some('=') if self.matches('=') => self.make_token(TokenType::EqEq),
            Some('=') => self.make_token(TokenType::Equal),
            Some('>') if self.matches('=') => self.make_token(TokenType::GreaterEq),
            Some('>') => self.make_token(TokenType::Greater),
            Some('<') if self.matches('=') => self.make_token(TokenType::LessEq),
            Some('<') => self.make_token(TokenType::Less),
            Some('"') => self.string(),
            Some(c) if c.is_numeric() => self.number(),
            Some(c) if c.is_alphabetic() => self.identifier(),
            _ => self.make_token(TokenType::Error),
        }
    }

    // return true if scanner is at end of source
    fn is_at_end(&self) -> bool {
        self.current == self.source.len()
    }

    // look at the current char without consuming it
    fn peek(&self) -> Option<char> {
        if self.is_at_end() {
            return None;
        }

        self.source.chars().nth(self.current)
    }

    // look past the current char without consuming it
    fn peek_next(&self) -> Option<char> {
        self.source.chars().nth(self.current + 1)
    }

    // Consume the current char
    fn advance(&mut self) -> Option<char> {
        let c = self.source.chars().nth(self.current);
        self.current += 1;
        c
    }

    // if the current char matches expected, consume it and return true
    // else return false
    fn matches(&mut self, expected: char) -> bool {
        if self.peek() == Some(expected) {
            self.current += 1;
            return true;
        }
        false
    }

    // consume any whitespace
    fn skip_comemnts_and_whitespace(&mut self) {
        while let Some(c) = self.peek() {
            match c {
                // Single line comment
                '/' if self.peek_next() == Some('/') => {
                    while !self.is_at_end() && self.peek() != Some('\n') {
                        self.advance();
                    }
                }
                // Multi-line comment
                '/' if self.peek_next() == Some('*') => {
                    while !self.is_at_end()
                        && (self.peek() != Some('*') || self.peek_next() != Some('/'))
                    {
                        self.advance();
                        if self.peek() == Some('\n') {
                            self.line += 1;
                        }
                    }
                    // Consume the closing */
                    self.advance();
                    self.advance();
                }
                // Other whitespace
                _ if c.is_whitespace() => {
                    if c == '\n' {
                        self.line += 1;
                    }
                    self.advance();
                }
                // Else we are done
                _ => return,
            }
        }
    }

    fn string(&mut self) -> Token {
        while !self.is_at_end() && self.peek() != Some('"') {
            self.advance();
            if self.peek() == Some('\n') {
                self.line += 1;
            }
        }

        if self.is_at_end() {
            return self.make_token(TokenType::Error);
        }

        // Consume closing '"'
        self.advance();
        self.make_token(TokenType::String)
    }

    fn number(&mut self) -> Token {
        while let Some(c) = self.peek() {
            if c.is_numeric() {
                self.advance();
            } else {
                break;
            }
        }

        if self.matches('.') {
            while let Some(c) = self.peek() {
                if c.is_numeric() {
                    self.advance();
                } else {
                    break;
                }
            }
        }

        self.make_token(TokenType::Number)
    }

    fn identifier(&mut self) -> Token {
        while let Some(c) = self.peek() {
            if c.is_alphanumeric() {
                self.advance();
            } else {
                break;
            }
        }
        self.make_token(self.identifier_type())
    }

    fn identifier_type(&self) -> TokenType {
        match self.get_source().as_str() {
            "and" => TokenType::And,
            "class" => TokenType::Class,
            "else" => TokenType::Else,
            "false" => TokenType::False,
            "for" => TokenType::For,
            "fun" => TokenType::Fun,
            "if" => TokenType::If,
            "nil" => TokenType::Nil,
            "or" => TokenType::Or,
            "print" => TokenType::Print,
            "return" => TokenType::Return,
            "super" => TokenType::Super,
            "this" => TokenType::This,
            "true" => TokenType::True,
            "var" => TokenType::Var,
            "while" => TokenType::While,
            _ => TokenType::Identifier,
        }
    }

    // Get the source snippet associated with a token
    pub fn get_lexeme(&self, token: &Token) -> &'source str {
        &self.source[token.start..token.end]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_single_char_tokens() {
        let mut scanner = Scanner::new("(){},.-+;*/");
        assert_eq!(scanner.scan_token().kind, TokenType::LeftParen);
        assert_eq!(scanner.scan_token().kind, TokenType::RightParen);
        assert_eq!(scanner.scan_token().kind, TokenType::LeftBrace);
        assert_eq!(scanner.scan_token().kind, TokenType::RightBrace);
        assert_eq!(scanner.scan_token().kind, TokenType::Comma);
        assert_eq!(scanner.scan_token().kind, TokenType::Dot);
        assert_eq!(scanner.scan_token().kind, TokenType::Minus);
        assert_eq!(scanner.scan_token().kind, TokenType::Plus);
        assert_eq!(scanner.scan_token().kind, TokenType::Semicolon);
        assert_eq!(scanner.scan_token().kind, TokenType::Star);
        assert_eq!(scanner.scan_token().kind, TokenType::Slash);
    }

    #[test]
    fn test_single_and_double_tokens() {
        let mut scanner = Scanner::new("! != = == > >= < <=");
        assert_eq!(scanner.scan_token().kind, TokenType::Bang);
        assert_eq!(scanner.scan_token().kind, TokenType::BangEq);
        assert_eq!(scanner.scan_token().kind, TokenType::Equal);
        assert_eq!(scanner.scan_token().kind, TokenType::EqEq);
        assert_eq!(scanner.scan_token().kind, TokenType::Greater);
        assert_eq!(scanner.scan_token().kind, TokenType::GreaterEq);
        assert_eq!(scanner.scan_token().kind, TokenType::Less);
        assert_eq!(scanner.scan_token().kind, TokenType::LessEq);
    }

    #[test]
    fn test_literal_tokens() {
        let mut scanner = Scanner::new("identifier \"string\" 0.0");
        assert_eq!(
            scanner.scan_token(),
            Token {
                kind: TokenType::Identifier,
                line: 0,
                start: 0,
                end: 10,
            }
        );
        assert_eq!(
            scanner.scan_token(),
            Token {
                kind: TokenType::String,
                line: 0,
                start: 11,
                end: 19,
            }
        );

        assert_eq!(
            scanner.scan_token(),
            Token {
                kind: TokenType::Number,
                line: 0,
                start: 20,
                end: 23,
            }
        );
    }

    #[test]
    fn test_keyword_tokens() {
        let mut scanner = Scanner::new(
            "and class else false for fun if nil or print return super this true var while",
        );

        assert_eq!(scanner.scan_token().kind, TokenType::And);
        assert_eq!(scanner.scan_token().kind, TokenType::Class);
        assert_eq!(scanner.scan_token().kind, TokenType::Else);
        assert_eq!(scanner.scan_token().kind, TokenType::False);
        assert_eq!(scanner.scan_token().kind, TokenType::For);
        assert_eq!(scanner.scan_token().kind, TokenType::Fun);
        assert_eq!(scanner.scan_token().kind, TokenType::If);
        assert_eq!(scanner.scan_token().kind, TokenType::Nil);
        assert_eq!(scanner.scan_token().kind, TokenType::Or);
        assert_eq!(scanner.scan_token().kind, TokenType::Print);
        assert_eq!(scanner.scan_token().kind, TokenType::Return);
        assert_eq!(scanner.scan_token().kind, TokenType::Super);
        assert_eq!(scanner.scan_token().kind, TokenType::This);
        assert_eq!(scanner.scan_token().kind, TokenType::True);
        assert_eq!(scanner.scan_token().kind, TokenType::Var);
        assert_eq!(scanner.scan_token().kind, TokenType::While);
    }

    #[test]
    fn test_skip_comments() {
        let mut scanner = Scanner::new("/* Nothing here*/ // and also nothing here");
        assert_eq!(scanner.scan_token().kind, TokenType::Eof);
    }
}

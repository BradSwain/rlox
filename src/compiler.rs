use crate::chunk::Chunk;
use crate::chunk::Op;
use crate::scanner::{Scanner, Token, TokenType};

/// Parse Precedence
#[derive(PartialEq, PartialOrd)]
enum Precedence {
    None,
    /// e.g. =
    Assignment,
    Or,
    And,
    /// e.g. == and !=
    Equality,
    /// e.g. >=, <=, <, >
    Comparison,
    /// e.g. + -
    Term,
    /// e.g. * /
    Factor,
    /// e.g. ! -
    Unary,
    /// . ()
    Call,
    Primary,
}

impl Precedence {
    // get the next higher precedence
    pub fn next(&self) -> Precedence {
        // TODO: probably a better way to do this
        match self {
            Self::None => Self::Assignment,
            Self::Assignment => Self::Or,
            Self::Or => Self::And,
            Self::And => Self::Equality,
            Self::Equality => Self::Comparison,
            Self::Comparison => Self::Term,
            Self::Term => Self::Factor,
            Self::Factor => Self::Unary,
            Self::Unary => Self::Call,
            Self::Call => Self::Primary,
            Self::Primary => unreachable!(),
        }
    }
}

struct Parser<'a, 'src> {
    // Chunk being built
    chunk: &'a mut Chunk,
    // Scanner
    scanner: Scanner<'src>,
    current: Token,
    prev: Token,
    had_error: bool,
}

impl<'a, 'src> Parser<'a, 'src> {
    fn new(code: &'src str, chunk: &'a mut Chunk) -> Parser<'a, 'src> {
        Parser {
            chunk,
            scanner: Scanner::new(code),
            current: Token {
                kind: TokenType::Error,
                end: 0,
                line: 0,
                start: 0,
            },
            prev: Token {
                kind: TokenType::Error,
                end: 0,
                line: 0,
                start: 0,
            },
            had_error: false,
        }
    }

    // Get the next non-error token from the scanner
    fn advance(&mut self) {
        self.prev = self.current;

        loop {
            self.current = self.scanner.scan_token();
            if self.current.kind != TokenType::Error {
                break;
            }
            self.error(self.current, "");
        }
    }

    // Consume token if it matches the type, else error
    fn consume(&mut self, kind: TokenType, msg: &str) {
        if self.current.kind == kind {
            self.advance();
            return;
        }
        self.error(self.current, msg);
    }

    // Print an error for the token
    fn error(&mut self, token: Token, msg: &str) {
        let at = match token.kind {
            TokenType::Eof => " at end".to_string(),
            TokenType::Error => "".to_string(),
            _ => format!("at '{}'", token.start),
        };
        println!("[line {}] Error{}: {}", token.line, at, msg);
        self.had_error = true;
    }

    fn number(&mut self) {
        match self.prev.kind {
            TokenType::Number => {
                let lexeme = self.scanner.get_lexeme(&self.prev);
                let n = lexeme.parse::<f64>().unwrap(); // TODO: handle error
                let id = self.chunk.add_constant(n);
                self.chunk.add_op(Op::Constant(id), self.prev.line);
            }
            _ => self.error(self.prev, "Expected number"),
        }
    }

    fn unary(&mut self) {
        let op_ty = self.prev.kind;

        self.parse_precedence(Precedence::Unary);

        match op_ty {
            TokenType::Minus => self.chunk.add_op(Op::Negate, self.prev.line),
            x => unreachable!("illegal unary op: {:?}", x),
        }
    }

    fn binary(&mut self) {
        let op_ty = self.prev.kind;

        let rule = self.get_rule(op_ty);
        self.parse_precedence(rule.precedence.next());

        match op_ty {
            TokenType::Plus => self.chunk.add_op(Op::Add, self.prev.line),
            TokenType::Minus => self.chunk.add_op(Op::Subtract, self.prev.line),
            TokenType::Star => self.chunk.add_op(Op::Multiply, self.prev.line),
            TokenType::Slash => self.chunk.add_op(Op::Divide, self.prev.line),
            x => unreachable!("illegal binop: {:?}", x),
        }
    }

    fn grouping(&mut self) {
        self.expression();
        self.consume(TokenType::RightParen, "Expect ')' after expression");
    }

    fn expression(&mut self) {
        self.parse_precedence(Precedence::Assignment);
    }

    fn parse_precedence(&mut self, precedence: Precedence) {
        self.advance();

        if let Some(prefix_rule) = self.get_rule(self.prev.kind).prefix {
            prefix_rule(self);
        }

        while precedence <= self.get_rule(self.current.kind).precedence {
            self.advance();
            if let Some(infix_rule) = self.get_rule(self.prev.kind).postfix {
                infix_rule(self);
            }
        }
    }

    // Get the parse rule for a given token type
    fn get_rule(&self, kind: TokenType) -> ParseRule<'a, 'src> {
        match kind {
            TokenType::LeftParen => ParseRule {
                prefix: Some(Parser::grouping),
                ..ParseRule::default()
            },
            TokenType::Minus => ParseRule {
                prefix: Some(Parser::unary),
                postfix: Some(Parser::binary),
                precedence: Precedence::Term,
            },
            TokenType::Plus => ParseRule {
                postfix: Some(Parser::binary),
                precedence: Precedence::Term,
                ..ParseRule::default()
            },
            TokenType::Slash => ParseRule {
                postfix: Some(Parser::binary),
                precedence: Precedence::Factor,
                ..ParseRule::default()
            },
            TokenType::Star => ParseRule {
                postfix: Some(Parser::binary),
                precedence: Precedence::Factor,
                ..ParseRule::default()
            },
            TokenType::Number => ParseRule {
                prefix: Some(Parser::number),
                ..ParseRule::default()
            },
            _ => ParseRule::default(),
        }
    }
}

type ParseFn<'a, 'src> = fn(&mut Parser<'a, 'src>);
struct ParseRule<'a, 'src> {
    prefix: Option<ParseFn<'a, 'src>>,
    postfix: Option<ParseFn<'a, 'src>>,
    precedence: Precedence,
}

impl<'a, 'src> Default for ParseRule<'a, 'src> {
    fn default() -> Self {
        ParseRule {
            prefix: None,
            postfix: None,
            precedence: Precedence::None,
        }
    }
}

pub fn compile(source: &str) -> Chunk {
    let mut chunk = Chunk::new();
    let mut parser = Parser::new(source, &mut chunk);
    parser.advance();

    parser.expression();

    chunk
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn negate() {
        let chunk = compile("-1");
        let mut it = chunk.code.iter();

        assert_eq!(Op::Constant(0), *it.next().unwrap());
        assert_eq!(1.0, *chunk.get_contant(0).unwrap());
        assert_eq!(Op::Negate, *it.next().unwrap());
    }

    #[test]
    fn add() {
        let chunk = compile("3 + 4");
        let expected = vec![Op::Constant(0), Op::Constant(1), Op::Add];

        assert_eq!(3.0, *chunk.get_contant(0).unwrap());
        assert_eq!(4.0, *chunk.get_contant(1).unwrap());

        assert_eq!(chunk.code.len(), expected.len());
        for (actual, expected) in chunk.code.iter().zip(expected.iter()) {
            assert_eq!(actual, expected);
        }
    }

    #[test]
    fn subtract() {
        let chunk = compile("3 - 4");
        let expected = vec![Op::Constant(0), Op::Constant(1), Op::Subtract];

        assert_eq!(3.0, *chunk.get_contant(0).unwrap());
        assert_eq!(4.0, *chunk.get_contant(1).unwrap());

        assert_eq!(chunk.code.len(), expected.len());
        for (actual, expected) in chunk.code.iter().zip(expected.iter()) {
            assert_eq!(actual, expected);
        }
    }

    #[test]
    fn multiply() {
        let chunk = compile("3 * 4");
        let expected = vec![Op::Constant(0), Op::Constant(1), Op::Multiply];

        assert_eq!(3.0, *chunk.get_contant(0).unwrap());
        assert_eq!(4.0, *chunk.get_contant(1).unwrap());

        assert_eq!(chunk.code.len(), expected.len());
        for (actual, expected) in chunk.code.iter().zip(expected.iter()) {
            assert_eq!(actual, expected);
        }
    }

    #[test]
    fn divide() {
        let chunk = compile("3 / 4");
        let expected = vec![Op::Constant(0), Op::Constant(1), Op::Divide];

        assert_eq!(3.0, *chunk.get_contant(0).unwrap());
        assert_eq!(4.0, *chunk.get_contant(1).unwrap());

        assert_eq!(chunk.code.len(), expected.len());
        for (actual, expected) in chunk.code.iter().zip(expected.iter()) {
            assert_eq!(actual, expected);
        }
    }

    #[test]
    fn grouping() {
        let chunk = compile("( (1+2) - ((-3)+4) )");
        println!("{}", chunk);

        let expected = vec![
            Op::Constant(0),
            Op::Constant(1),
            Op::Add,
            Op::Constant(2),
            Op::Negate,
            Op::Constant(3),
            Op::Add,
            Op::Subtract,
        ];

        assert_eq!(expected.len(), chunk.code.len());
        for (actual, expected) in chunk.code.iter().zip(expected.iter()) {
            assert_eq!(actual, expected);
        }

        assert_eq!(1.0, *chunk.get_contant(0).unwrap());
        assert_eq!(2.0, *chunk.get_contant(1).unwrap());
        assert_eq!(3.0, *chunk.get_contant(2).unwrap());
        assert_eq!(4.0, *chunk.get_contant(3).unwrap());
    }
}

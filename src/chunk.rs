use crate::value::{Value, ValueArray};
use std::fmt;

pub type ConstantID = usize;

// Op is a single instruction or "Operation"
#[derive(Debug, PartialEq)]
pub enum Op {
    Return,
    Constant(ConstantID),
    Negate,
    Add,
    Subtract,
    Multiply,
    Divide,
}

// Chunk represents a sequence of bytecode ops
pub struct Chunk {
    pub code: Vec<Op>,
    constants: ValueArray,
    // track line number of Ops in code
    lines: Vec<usize>,
}

impl Chunk {
    // Create a new empty Chunk
    pub fn new() -> Chunk {
        Chunk {
            code: Vec::new(),
            constants: ValueArray::new(),
            lines: Vec::new(),
        }
    }

    // Add an operation ot the end of the chunk
    pub fn add_op(&mut self, op: Op, line: usize) {
        self.code.push(op);
        self.lines.push(line);
    }

    // Add constant to chunk's constants and return it's identifier
    pub fn add_constant(&mut self, val: Value) -> ConstantID {
        self.constants.push(val);
        self.constants.len() - 1
    }

    // Get a constant from the chunk's list of constants by id
    pub fn get_contant(&self, id: ConstantID) -> Option<&Value> {
        self.constants.get(id)
    }
}

impl fmt::Display for Chunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "===Chunk===")?;
        for (i, op) in self.code.iter().enumerate() {
            if i > 0 && self.lines[i] == self.lines[i - 1] {
                write!(f, "   | ")?;
            } else {
                write!(f, "{:>width$} ", self.lines[i], width = 4)?;
            }
            writeln!(f, "{}", op)?;
        }
        writeln!(f, "   Constants")?;
        for (i, v) in self.constants.iter().enumerate() {
            writeln!(f, "    {}: {}", i, v)?;
        }
        Ok(())
    }
}

impl fmt::Display for Op {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Op::Return => write!(f, "OP_Return"),
            Op::Constant(v) => write!(f, "OP_Constant {}", v),
            Op::Negate => write!(f, "OP_Negate"),
            Op::Add => write!(f, "OP_Add"),
            Op::Subtract => write!(f, "OP_Subtract"),
            Op::Multiply => write!(f, "OP_Multiply"),
            Op::Divide => write!(f, "OP_Divide"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::value::Value;

    #[test]
    fn adding_ops() {
        let mut chunk = Chunk::new();

        let id = chunk.add_constant(1.23);
        chunk.add_op(Op::Constant(id), 0);
        chunk.add_op(Op::Return, 0);

        let mut it = chunk.code.iter();
        assert_eq!(Op::Constant(id), *it.next().unwrap());
        assert_eq!(*chunk.get_contant(id).unwrap(), 1.23);
        assert_eq!(Op::Return, *it.next().unwrap());
    }

    #[test]
    fn adding_constants() {
        let mut chunk = Chunk::new();

        chunk.add_constant(1.23);

        let mut it = chunk.constants.iter();
        assert_eq!(1.23 as Value, *it.next().unwrap());
    }
}

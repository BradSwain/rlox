use crate::chunk::Chunk;
use crate::chunk::Op;
use crate::compiler::compile;
use crate::value::Value;

pub struct VM<'src> {
    chunk: &'src Chunk,
    stack: Vec<Value>,
}

#[derive(Debug)]
pub enum InterpretErr {
    CompilerErr,
    RuntimeErr,
}

pub type InterpretResult = Result<(), InterpretErr>;

impl<'src> VM<'src> {
    pub fn new(chunk: &'src Chunk) -> VM<'src> {
        VM {
            chunk,
            stack: Vec::new(),
        }
    }

    pub fn run(&mut self) -> InterpretResult {
        for op in self.chunk.code.iter() {
            #[cfg(feature = "debug_trace")]
            {
                for val in self.stack.iter() {
                    print!("[{}]", val);
                }
                println!("");
                println!("{}", op);
            }

            match *op {
                Op::Return => return Ok(()),
                Op::Constant(id) => {
                    self.stack.push(*self.chunk.get_contant(id).unwrap());
                }
                Op::Negate => {
                    if let Some(val) = self.stack.pop() {
                        self.stack.push(-val);
                    } else {
                        return Err(InterpretErr::RuntimeErr);
                    }
                }
                Op::Add => {
                    let lhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let rhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let result: Value = lhs + rhs;
                    self.stack.push(result);
                }
                Op::Subtract => {
                    let lhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let rhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let result: Value = lhs - rhs;
                    self.stack.push(result);
                }
                Op::Multiply => {
                    let lhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let rhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let result: Value = lhs * rhs;
                    self.stack.push(result);
                }
                Op::Divide => {
                    let lhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let rhs = self.stack.pop().ok_or(InterpretErr::RuntimeErr)?;
                    let result: Value = lhs / rhs;
                    self.stack.push(result);
                }
            }
        }
        Ok(())
    }
}

pub fn interpret(source: &str) -> InterpretResult {
    let chunk = compile(source);

    let mut vm = VM::new(&chunk);
    vm.run()
}

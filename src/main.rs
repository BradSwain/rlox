mod chunk;
mod compiler;
mod scanner;
mod value;
mod vm;

use std::env;
use std::fs::read_to_string;
use std::io::BufRead;

use vm::interpret;

fn read_from_file(path: &str) -> String {
    read_to_string(path).unwrap()
}

fn main() {
    println!("Welcome to Brad's Programming Language!");

    // Execute file
    if env::args().len() >= 1 {
        let path = &env::args().nth(1).unwrap();
        let source = read_from_file(path);
        let result = interpret(&source);
        match result {
            Ok(_) => std::process::exit(0),
            Err(err) => {
                println!("Err: {:?}", err);
                std::process::exit(1)
            }
        };
    }

    // REPL
    let mut line = String::new();
    let mut eof = false;
    while !eof {
        match std::io::stdin().lock().read_line(&mut line) {
            Ok(0) => {
                eof = true;
            }
            Ok(_) => {
                let result = interpret(&line);
                match result {
                    Ok(_) => {}
                    Err(err) => {
                        println!("Err: {:?}", err);
                    }
                };
            }
            Err(e) => panic!("{}", e),
        }
    }
}
